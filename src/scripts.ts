enum Estados { vivo = 0, muerto = 1 }

interface Senyor {
  id: number;
  avatar: string;
  nombre: string;
  profesion: string;
  estado: Estados;
  twitter: string;
  chequeado: boolean;
}

let senyoresMockup: Array<Senyor> = [
  {
    id: 12,
    avatar: 'fary.jpg',
    nombre: 'El Fary',
    profesion: 'Influencer',
    estado: Estados.muerto,
    twitter: 'Pendiente',
    chequeado: true
  },
  {
    id: 20,
    avatar: 'julio.jpg',
    nombre: 'Julio Iglesias',
    profesion: 'Youtuber',
    estado: Estados.vivo,
    twitter: '@JulioIglesias',
    chequeado: false
  },
  {
    id: 32,
    avatar: 'bertin.jpg',
    nombre: 'Bertín Osborne',
    profesion: 'Java Developer',
    estado: Estados.vivo,
    twitter: '@BertinOsborne',
    chequeado: false
  },
  {
    id: 40,
    avatar: 'tio.jpg',
    nombre: 'Tío desconocido',
    profesion: 'Señalador',
    estado: Estados.vivo,
    twitter: '@ContratamePorFavor',
    chequeado: true
  }
];

class AppSenyores {

  private senyores: Array<Senyor>;
  private $senyorBase: HTMLElement;
  private $contenedorListado: HTMLElement;
  private $botonMarcar: HTMLElement;
  private estadoBotonMarcarDesmarcar = 'marcar';
  private textoBotonMarcarDesmarcar = 'Marcar todos';
  private dirImgs = 'img/';

  constructor() {
    this.senyores = this.getSenyoresAPI();
    this.inicializarHTML();
    this.rellenaLista();
    this.actualizaTotal();
    this.escuchaBoton();
  }

  private getSenyoresAPI(): Array<Senyor> {
    return senyoresMockup;
  }

  private escuchaBoton(): void {
    this.$botonMarcar.addEventListener('click', () => {
      this.marcarDesmarcarTodos(this.estadoBotonMarcarDesmarcar === 'marcar');
    });
  }

  private marcarDesmarcarTodos(marcar: boolean): void {
    for (let senyor of this.senyores) {
      senyor.chequeado = marcar;
    }
    this.rellenaLista();
    this.estadoBotonMarcarDesmarcar = this.estadoBotonMarcarDesmarcar === 'marcar' ? 'desmarcar' : 'marcar';
    this.textoBotonMarcarDesmarcar = this.estadoBotonMarcarDesmarcar === 'marcar' ? 'Marcar todos' : 'Desmarcar todos';
    this.$botonMarcar.textContent = this.textoBotonMarcarDesmarcar;
    this.actualizaTotal();
  }

  private imprimeEstado(estado: Estados): string {
    let estados = {
      0: 'Vivo',
      1: 'R.I.P.'
    }
    return estados[estado];
  }

  private actualizaTotal(): void {
    let $total = document.querySelector('.n-senyores');
    $total.textContent = this.getTotalMarcados();
  }

  private getTotalMarcados(): string {
    let total = this.senyores.filter(senyor => senyor.chequeado).length;
    return total.toString();
  }

  private vaciaLista(): void {
    let $senyores = this.$contenedorListado.querySelectorAll('.senyor');
    if ($senyores.length > 0) {
      for (let i = 0; i < $senyores.length; i++) {
        $senyores[i].remove();
      }
    }
  }

  private rellenaLista(): void {
    this.$contenedorListado = document.querySelector('.contenedor-listado');
    this.vaciaLista();

    for (let senyor of this.senyores) {
      let $nuevoSenyor = this.rellenaSenyorHTML(senyor);
      this.$contenedorListado.appendChild($nuevoSenyor);
    }
  }

  private rellenaSenyorHTML(senyor: Senyor): HTMLElement {
    let $nuevoSenyor = <HTMLElement>this.$senyorBase.cloneNode(true);
    $nuevoSenyor.classList.remove('oculto', 'base');
    let $nombre = $nuevoSenyor.querySelector('.nombre');
    $nombre.textContent = senyor.nombre;
    let $profesion = $nuevoSenyor.querySelector('.profesion');
    $profesion.textContent = senyor.profesion;
    let $twitter = $nuevoSenyor.querySelector('.twitter');
    $twitter.textContent = senyor.twitter;
    let $estado = $nuevoSenyor.querySelector('.estado');
    $estado.textContent = this.imprimeEstado(senyor.estado);
    let $inicial = $nuevoSenyor.querySelector('.inicial');
    $inicial.textContent = senyor.nombre.charAt(0).toUpperCase();
    let $marcado = $nuevoSenyor.querySelector('.marcado');
    let $contenedorFoto = $nuevoSenyor.querySelector('.avatar');
    let $foto = $contenedorFoto.querySelector('img');
    $foto.setAttribute('src', `${this.dirImgs}${senyor.avatar}`);
    if (senyor.chequeado) {
      $contenedorFoto.classList.add('marcado');
      $marcado.classList.remove('oculto');
    } else {
      $contenedorFoto.classList.remove('marcado');
      $marcado.classList.add('oculto');
    }
    $nuevoSenyor.setAttribute('id', `senyor-${senyor.id}`);
    this.anyadeClickListener($nuevoSenyor);
    return $nuevoSenyor;
  }

  private anyadeClickListener($senyor: HTMLElement): void {
    $senyor.addEventListener('click', (e) => {
      let $elemento = <HTMLElement>e.target;
      let $senyor = $elemento.closest('.senyor');
      let idCompleta = $senyor.getAttribute('id').split('-');
      let idSenyor = Number(idCompleta.pop());

      let senyor = this.senyores.find(senyor => senyor.id === idSenyor);
      senyor.chequeado = !senyor.chequeado;
      this.rellenaLista();
      this.actualizaTotal();
    });
  }

  private inicializarHTML(): void {
    this.$senyorBase = document.querySelector('.senyor.base');
    this.$botonMarcar = document.querySelector('.marcar button');
    this.$botonMarcar.textContent = this.textoBotonMarcarDesmarcar;
  }

}

new AppSenyores();
